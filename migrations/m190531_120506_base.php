<?php

use yii\db\Migration;

/**
 * Class m190531_120506_base
 */
class m190531_120506_base extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('medicament', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
        $this->createTable('component', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'visible' => $this->boolean()->defaultValue(true),
        ]);
        $this->createTable('medicament_component', [
            'medicament_id' => $this->integer()->notNull(),
            'component_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'fk-medicament_component-medicament_id',
            'medicament_component',
            'medicament_id',
            'medicament',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk-medicament_component-component_id',
            'medicament_component',
            'component_id',
            'component',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('medicament_component');
        $this->dropTable('medicament');
        $this->dropTable('component');
    }
}
