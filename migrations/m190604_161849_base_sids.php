<?php

use yii\db\Migration;

/**
 * Class m190604_161849_base_sids
 */
class m190604_161849_base_sids extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->batchInsert('component', ['name'], [
            ['ромашка'],
            ['спирт'],
            ['меланин'],
            ['женьшень'],
            ['кальций'],
            ['магний'],
            ['натрий'],
            ['кремний'],
            ['иод'],
            ['крахмал'],
            ['кислота памидроновая'],
            ['аскорбиновая кислота'],
            ['витамин B1'],
            ['витамин B6'],
            ['витамин B12'],
        ]);

        $this->batchInsert('medicament', ['name'], [
            ['Настойка женьшени'],
            ['Настойка ромашки'],
            ['Глюконат кальция'],
            ['Йодомарин'],
            ['Витамин C'],
        ]);

        $this->batchInsert('medicament_component', ['medicament_id', 'component_id'], [
            [1, 4],
            [1, 2],
            [1, 3],

            [2, 1],
            [2, 2],
            [2, 15],

            [3, 5],
            [3, 6],
            [3, 7],
            [3, 12],
            [3, 15],

            [4, 9],
            [4, 5],
            [4, 15],

            [5, 2],
            [5, 12],
            [5, 4],
            [5, 14],
            [5, 15],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        Yii::$app->db->createCommand()->checkIntegrity(false)->execute();
        Yii::$app->db->createCommand()->truncateTable('component')->execute();
        Yii::$app->db->createCommand()->truncateTable('medicament')->execute();
        Yii::$app->db->createCommand()->truncateTable('medicament_component')->execute();
        Yii::$app->db->createCommand()->checkIntegrity(true)->execute();


    }
}
