<?php

namespace app\modules\medic\models\ar;

use Yii;
use yii\db\ActiveQuery;

class MedicamentQuery extends \yii\db\ActiveQuery
{
    /**
     * @return MedicamentQuery
     */
    public function visibleComponents()
    {
        $subQuery = MedicamentComponent::find()->select('medicament_id')
                                       ->joinWith('component')
                                       ->where(['component.visible' => 0]);

        return $this->where(['not in','id', $subQuery]);
    }
}