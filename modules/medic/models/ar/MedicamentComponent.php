<?php

namespace app\modules\medic\models\ar;

use Yii;

/**
 * This is the model class for table "medicament_component".
 *
 * @property int $medicament_id
 * @property int $component_id
 *
 * @property Component $component
 * @property Medicament $medicament
 */
class MedicamentComponent extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medicament_component';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['medicament_id', 'component_id'], 'required'],
            [['medicament_id', 'component_id'], 'integer'],
            [['component_id'], 'exist', 'skipOnError' => true, 'targetClass' => Component::className(), 'targetAttribute' => ['component_id' => 'id']],
            [['medicament_id'], 'exist', 'skipOnError' => true, 'targetClass' => Medicament::className(), 'targetAttribute' => ['medicament_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'medicament_id' => 'Medicament ID',
            'component_id' => 'Component ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponent()
    {
        return $this->hasOne(Component::className(), ['id' => 'component_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedicament()
    {
        return $this->hasOne(Medicament::className(), ['id' => 'medicament_id']);
    }
}
