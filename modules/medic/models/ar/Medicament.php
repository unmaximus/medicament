<?php

namespace app\modules\medic\models\ar;

use Yii;

/**
 * This is the model class for table "medicament".
 *
 * @property int $id
 * @property string $name
 *
 * @property MedicamentComponent[] $medicamentComponents
 * @property Component[] $components
 */
class Medicament extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'medicament';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMedicamentComponents()
    {
        return $this->hasMany(MedicamentComponent::className(), ['medicament_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComponents()
    {
        return $this->hasMany(Component::className(), ['id' => 'component_id'])->via('medicamentComponents');
    }

    /**
     * @return MedicamentQuery|\yii\db\ActiveQuery
     */
    public static function find() {
        return (new MedicamentQuery(get_called_class()))
            ->visibleComponents();
    }
}
