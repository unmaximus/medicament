<?php

namespace app\modules\medic\models\form;

use app\modules\medic\models\ar\Component;
use app\modules\medic\models\ar\Medicament;
use app\modules\medic\models\MedicamentFinder;
use yii\helpers\ArrayHelper;

/**
 * Class BaseMedicamentForm
 * @package app\modules\medic\models\form
 *
 * @property array $paramsComponent
 * @property string $messageCountError
 */
abstract class BaseMedicamentForm extends Medicament {

    public $paramsComponent;

    public $messageCountError = 'не ленись, добавь веществ';

    /**
     * @return array
     */
    public function rules() {
        return ArrayHelper::merge(parent::rules(),[
            ['paramsComponent', 'filter', 'filter' => [$this, 'normalizeParams']],
            ['paramsComponent', 'componentsValidate'],
        ]);
    }

    /**
     * @param $value
     *
     * @return array
     */
    public function normalizeParams($params) {
        $params = array_unique($params);

        return array_filter($params, function($item) { return $item !== ''; });
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function componentsValidate($attribute, $params) {
        if (count($this->$attribute) < MedicamentFinder::MIN_COMPONENT) {
            $this->addError($attribute, $this->messageCountError);
        }
    }

    /**
     * @return array
     */
    public function getValueText() {
        if (empty($this->paramsComponent)) {
            return [];
        }
        $components = Component::findAll(['id' => $this->paramsComponent]);
        $components = ArrayHelper::map($components, 'id', 'name');

        $order = $this->paramsComponent;
        uksort($components, function($key1, $key2) use ($order) {
            return ((array_search($key1, $order) > array_search($key2, $order)) ? 1 : -1);
        });

        return $components;
    }

}