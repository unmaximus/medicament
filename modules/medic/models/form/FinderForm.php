<?php

namespace app\modules\medic\models\form;


/**
 * Class FinderForm
 * @package app\modules\medic\models\form
 *
 * @property array $paramsComponent
 */
class FinderForm extends BaseMedicamentForm
{
    CONST MAX_COMPONENT = 5;

    public $paramsComponent;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['paramsComponent', 'filter', 'filter' => [$this, 'normalizeParams']],
            ['paramsComponent', 'filter', 'filter' => [$this, 'componentsMax']],
            [['paramsComponent'], 'required', 'message' => 'добавь веществ'],
            ['paramsComponent', 'componentsValidate'],
        ];
    }

    /**
     * @param $params
     *
     * @return mixed
     */
    public function componentsMax($params) {
        if (count($params) > self::MAX_COMPONENT) {
            array_splice($params, self::MAX_COMPONENT);
        }

        return $params;
    }

}
