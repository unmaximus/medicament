<?php

namespace app\modules\medic\models\form;

use app\modules\medic\models\ar\MedicamentComponent;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class MedicamentForm
 * @package app\modules\medic\models\form
 *
 * @property array $paramsComponent
 */
class MedicamentForm extends BaseMedicamentForm
{
    public $paramsComponent;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(),[
            [['name', 'paramsComponent'], 'required'],
            [['name'], 'string', 'max' => 255],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(),
            [
                'paramsComponent' => 'Компоненты',
                'components' => 'Компоненты',
            ]);
    }

    /**
     * @param bool $runValidation
     * @param null $attributeNames
     *
     * @return bool
     */
    public function save($runValidation = true, $attributeNames = null) {
        $transaction = Yii::$app->db->beginTransaction();

        try {
            $isNewRecord = $this->getIsNewRecord();

            if( ! parent::save($runValidation, $attributeNames))
                throw new \Exception;

            if( ! $this->saveComponentRelations($isNewRecord))
                throw new \Exception;

            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            return false;
        }

        return true;
    }

    /**
     * @param bool $isNewRecord
     *
     * @return bool
     */
    protected function saveComponentRelations($isNewRecord = true) {
        if ( ! $isNewRecord) {
            MedicamentComponent::deleteAll(['medicament_id' => $this->id]);
        }

        foreach ($this->paramsComponent as $componentId) {
            $medicamentOfComponent = new MedicamentComponent();
            $medicamentOfComponent->component_id = $componentId;
            $medicamentOfComponent->medicament_id = $this->id;

            if ( ! $medicamentOfComponent->save()) {
                $this->addError('paramsComponent', 'Ошибка связи с компонентом #' . $componentId);

                return false;
            }
        }

        return true;
    }

    public function afterFind() {
        parent::afterFind();

        if (!empty($this->id)) {
            $paramsComponent = MedicamentComponent::find()
                                             ->select('component_id')
                                             ->where(['medicament_id' => $this->id])
                                             ->asArray()->all();

            $this->paramsComponent = ArrayHelper::getColumn($paramsComponent, 'component_id');
        }
    }
}
