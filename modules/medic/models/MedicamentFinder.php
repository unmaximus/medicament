<?php

namespace app\modules\medic\models;

use app\modules\medic\models\ar\Medicament;
use app\modules\medic\models\form\FinderForm;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;
use yii\db\Query;

/**
 * Class MedicamentFinder
 * @package app\modules\medic\models
 */
class MedicamentFinder extends Model
{
    CONST MIN_COMPONENT = 2;

    public $params;

    /**
     * @return array
     */
    public function rules() {
        return [
            [['params',], 'each', 'rule' => ['integer']],
        ];
    }

    /**
     * @param FinderForm $form
     *
     * @return array
     */
    public function search(FinderForm $form) {
        $this->params = $form->paramsComponent;

        if ( ! $this->validate() || empty($this->params)) {
            return [];
        }

        $ids = implode(',', $this->params);

        $subQuery = Medicament::find()->alias('m')
            ->select([
                'm.id',
                'name' => 'MAX(m.name)',
                'count_match' => "SUM(CASE WHEN mc.component_id IN (" . $ids . ")
                    THEN 1 
                    ELSE 0 
                    END)",
                'count_component' => "COUNT(*)",
            ])
            ->joinWith(['medicamentComponents mc'], false)
            ->groupBy(['m.id'])
            ->having(['>=', 'count_match', self::MIN_COMPONENT]);


        $medicaments = (new Query())->select(['*'])
                                    ->from(['medicaments' => $subQuery])
                                    ->orderBy(['ABS(count_match - count_component) + ABS(count_match - ' . count($this->params) . ')' => SORT_ASC])
                                    ->all();

        $result = [];
        foreach ($medicaments as $item) {
            if ($item['count_match'] == $item['count_component']
                && $item['count_match'] == count($this->params)) {

                $result[] = $item;
            } else {
                break;
            }
        }

        if (empty($result)) {
            $result = $medicaments;
        }

        return ArrayHelper::map($result, 'id', 'name');
    }

}
