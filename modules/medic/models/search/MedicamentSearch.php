<?php

namespace app\modules\medic\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\modules\medic\models\ar\Medicament;
use yii\helpers\ArrayHelper;

/**
 * MedicamentSearch represents the model behind the search form of `app\modules\medic\models\ar\Medicament`.
 */
class MedicamentSearch extends Medicament
{
    public $components;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['name'], 'safe'],
            [['components'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Medicament::find()->alias('m')
                                   ->select(['m.*', 'COUNT(component_id) AS count_component'])
                                   ->joinWith('medicamentComponents', false)
                                   ->groupBy(['m.id', 'm.name']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);
        $dataProvider->sort->attributes['components'] = [
            'asc' => ['count_component' => SORT_ASC],
            'desc' => ['count_component' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name]);

        return $dataProvider;
    }
}
