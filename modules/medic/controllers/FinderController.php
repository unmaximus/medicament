<?php

namespace app\modules\medic\controllers;

use app\modules\medic\models\form\FinderForm;
use app\modules\medic\models\MedicamentFinder;
use yii\web\Controller;


class FinderController extends Controller
{
    /**
     * @return string
     */
    public function actionIndex()
    {
        $get = \Yii::$app->request->get();
        $model = new FinderForm();
        $result = null;

        if ($get && $model->load($get) && $model->validate()) {
            $modelFinder = new MedicamentFinder();
            $result = $modelFinder->search($model);
        }

        return $this->render('index', [
            'model' => $model,
            'medicament' => $result,
        ]);
    }
}
