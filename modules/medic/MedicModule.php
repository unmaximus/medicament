<?php

namespace app\modules\medic;

/**
 * medic module definition class
 */
class MedicModule extends \yii\base\Module
{
    public $layout = 'medic';

    public $defaultRoute = 'finder';

    /**
     * {@inheritdoc}
     */
    public $controllerNamespace = 'app\modules\medic\controllers';

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
