<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;

/* @var $medicament */
/* @var $model app\modules\medic\models\form\FinderForm  */
/* @var $form yii\widgets\ActiveForm */

$this->title = 'Medicine';

?>
<div class="container">
    <?php $form = ActiveForm::begin(['method' => 'GET', 'action' => '/',]); ?>

    <h4 class="form-signin-heading">Выберете компоненты лекартсва:</h4>

    <?=$form->field($model, 'paramsComponent')->widget(Select2::classname(), [
        'initValueText' => $model->getValueText(),
        'showToggleAll' => false,
        'language' => 'ru',
        'options' => ['multiple' => true, 'placeholder' => '...', 'label' =>''],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'maximumSelectionLength' => \app\modules\medic\models\form\FinderForm::MAX_COMPONENT,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['component/select-list']),
                'dataType' => 'json',
            ],
        ],
    ])->label('');?>

    <br>
    <button class="btn btn-lg btn-primary " type="submit">Поиск</button>
    <br>
    <br>

    <?php ActiveForm::end(); ?>

    <?php if (isset($medicament)) { ?>
        <hr>
        <?php if (!empty($medicament)) { ?>
            <h4>Найдены следующие лекартсва:</h4>
            <?php $i=0;
            foreach ($medicament as $item) { $i++; ?>
                <p class="lead"><?= $i?>) <?=$item?></p>
            <?php } ?>
        <?php } else { ?>
            <h4>не найдено лекарств</h4>
        <?php } ?>
    <?php } ?>

</div>