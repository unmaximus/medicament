<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\web\JsExpression;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\modules\medic\models\form\MedicamentForm */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="medicament-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'paramsComponent')->widget(Select2::classname(), [
        'name' => 'paramsComponent',
        'initValueText' => $model->getValueText(),
        'showToggleAll' => false,
        'language' => 'ru',
        'options' => ['multiple' => true, 'placeholder' => '...'],
        'pluginOptions' => [
            'allowClear' => true,
            'minimumInputLength' => 1,
            'ajax' => [
                'url' => \yii\helpers\Url::to(['component/select-list']),
                'dataType' => 'json',
            ],
        ],
    ]);?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
