<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\medic\models\search\MedicamentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Medicaments';
?>
<div class="container medicament-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Medicament', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'components' => [
                'format' => 'raw',
                'attribute' => 'components',
                'label' => 'Компоненты',
                'value' => function ($model) {
                    $components = \yii\helpers\ArrayHelper::getColumn($model->components, 'name');

                    return implode(', ',$components);
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
