<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\medic\models\ar\Medicament */

$this->title = 'Update Medicament: ' . $model->name;
?>
<div class="container medicament-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
