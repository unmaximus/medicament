<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\medic\models\ar\Medicament */

$this->title = 'Create Medicament';
?>
<div class="container medicament-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
