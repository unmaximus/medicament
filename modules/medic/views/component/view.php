<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\modules\medic\models\ar\Component */

$this->title = $model->name;

\yii\web\YiiAsset::register($this);
?>
<div class="container component-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'visible' => [
                'format' => 'raw',
                'attribute' => 'visible',
                'value' => function ($model) {
                    $class = $model->visible ? 'glyphicon-ok text-success' : 'glyphicon-remove text-danger';

                    return "<span class=\"glyphicon $class\"></span>";
                }
            ],
        ],
    ]) ?>

</div>
