<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\modules\medic\models\ar\Component */

$this->title = 'Update Component: ' . $model->name;
?>
<div class="container component-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
