<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\modules\medic\models\search\ComponentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Components';
?>
<div class="container component-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Component', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
        'columns' => [
            'id',
            'name',
            'visible' => [
                'format' => 'raw',
                'attribute' => 'visible',
                'value' => function ($model) {
                    $class = $model->visible ? 'glyphicon-ok text-success' : 'glyphicon-remove text-danger';

                    return "<span class=\"glyphicon $class\"></span>";
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
